﻿1
00:00:00,760 --> 00:00:04,987
Dies ist eine Einführung in das Intelligence-Tool von CrowdTangle.

2
00:00:04,987 --> 00:00:10,814
Intelligence liefert Statistiken auf Kontoebene mit Grafiken und Diagrammen für Daten eines bestimmten Zeitraums.

3
00:00:10,814 --> 00:00:15,174
Das Tool kann auf allen CrowdTangle-fähigen Plattformen verwendet werden,

4
00:00:15,174 --> 00:00:19,565
einschließlich Facebook, Instagram, Twitter und Reddit.

5
00:00:19,565 --> 00:00:23,301
Damit können Publisher sich abzeichnende Trends erkennen

6
00:00:23,301 --> 00:00:26,548
und analysieren, welcher Content gute Ergebnisse liefert

7
00:00:26,548 --> 00:00:28,737
und welcher Content nicht.

8
00:00:28,737 --> 00:00:34,901
Um Intelligence nutzen zu können, bindest du dein eigenes Konto in der Suchleiste oben ein.

9
00:00:34,901 --> 00:00:42,456
Du kannst nach Art des Posts (Fotos, Links, Videos) und nach Art der Interaktion

10
00:00:42,456 --> 00:00:45,602
(„Gefällt mir“-Angaben, Kommentare, geteilte Inhalte) sortieren.

11
00:00:45,602 --> 00:00:51,831
Du kannst bis zu 5 verschiedene Seiten in der Suchleiste oben einbinden

12
00:00:51,831 --> 00:00:54,956
und aussagekräftige Analysen erstellen.

13
00:00:54,956 --> 00:01:00,165
Außerdem kannst du diesen Bericht als PDF exportieren

14
00:01:00,165 --> 00:01:03,221
oder in Form einer CSV-Datei herunterladen.

15
00:01:03,221 --> 00:01:07,858
Du kannst den Bericht auch in einen öffentlichen Link verwandeln

16
00:01:07,858 --> 00:01:12,487
und mit deinem Team teilen – unabhängig davon, ob es CrowdTangle verwendet oder nicht.

17
00:01:13,861 --> 00:01:18,201
Falls du Fragen hast, kannst du dich gern an das CrowdTangle-Team wenden.

18
00:01:18,201 --> 00:01:21,219
Verwende dazu den Chat-Button unten rechts.

19
00:01:21,219 --> 00:01:24,086
Viel Spaß bei der Verwendung von CrowdTangle.

