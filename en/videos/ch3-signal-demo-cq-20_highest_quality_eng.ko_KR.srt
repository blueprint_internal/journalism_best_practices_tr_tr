﻿1
00:00:01,479 --> 00:00:05,136
뉴스, 엔터테인먼트, 스포츠를 비롯한 다양한 카테고리에서

2
00:00:05,136 --> 00:00:10,019
언론인들이 Facebook과 Instagram 콘텐츠를 찾아볼 수 있도록 고안된

3
00:00:10,019 --> 00:00:14,379
Signal의 기능을 소개해드립니다

4
00:00:14,379 --> 00:00:17,377
Signal 대시보드에서는 가장 많은 화제를 불러모으고 있는 주제를

5
00:00:17,377 --> 00:00:20,262
실시간으로 확인할 수 있습니다

6
00:00:20,956 --> 00:00:23,797
인기 주제는 인기도 순으로 표시됩니다

7
00:00:24,254 --> 00:00:27,531
궁금한 주제를 클릭하고 연관 게시물을 확인하여

8
00:00:27,531 --> 00:00:30,315
해당 주제에 관한 다양한 의견을 알아보세요

9
00:00:30,664 --> 00:00:35,365
카테고리 보기로 전환하면

10
00:00:35,365 --> 00:00:39,846
정치, 비즈니스, 엔터테인먼트, 스포츠 및 기타 도메인에서

11
00:00:39,846 --> 00:00:44,087
가장 많이 언급되는 유명인이 누구인지 확인할 수 있습니다

12
00:00:44,087 --> 00:00:47,634
Facebook에서 실시간으로 급상승 중인 주제를 확인하려면

13
00:00:47,634 --> 00:00:49,862
최신 주제 보기를 클릭하세요

14
00:00:50,684 --> 00:00:54,169
키워드 검색을 활용하여 Facebook 콘텐츠를 찾을 수도 있습니다

15
00:00:58,089 --> 00:01:02,222
결과는 역시간순으로 나타나므로

16
00:01:02,222 --> 00:01:04,222
최신 결과가 가장 상단에 표시됩니다

17
00:01:04,967 --> 00:01:07,679
해시태그, 위치 태그나 사용자 계정 이름을 검색하여

18
00:01:07,679 --> 00:01:12,518
Instagram의 시각적 콘텐츠를 찾을 수도 있습니다

19
00:01:15,425 --> 00:01:20,323
흥미로운 게시물을 발견하면

20
00:01:20,323 --> 00:01:22,924
컬렉션에 저장하여

21
00:01:23,806 --> 00:01:28,032
온라인 기사를 작성하거나 웹페이지를 만들 때 활용할 수도 있습니다

22
00:01:28,791 --> 00:01:30,872
게시물을 웹페이지에 추가하려면

23
00:01:30,872 --> 00:01:36,216
Embed 코드를 복사하여 여러분의 코드에 붙여넣기만 하면 됩니다

24
00:01:37,180 --> 00:01:40,879
Signal에는 본 데모 동영상에서 간단히 보여드린 것보다

25
00:01:40,879 --> 00:01:41,951
훨씬 더 많은 기능이 있습니다

26
00:01:41,951 --> 00:01:45,049
자세히 알아보려면 지금 바로 Signal을 사용해보세요

27
00:01:45,049 --> 00:01:50,685
Blueprint 과정의 마지막 챕터에 있는 링크를 사용하여 액세스를 요청하세요

28
00:01:53,217 --> 00:01:57,918
Signal 데모를 시청해주셔서 감사합니다


