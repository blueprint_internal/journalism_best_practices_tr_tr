﻿1
00:00:01,053 --> 00:00:05,238
Bu videoda, CrowdTangle Intelligence
aracına kısa bir giriş yapacağız.

2
00:00:05,238 --> 00:00:11,390
Intelligence, grafik ve tablolarla hesap düzeyinde
zamana göre istatistikler sunar.

3
00:00:11,390 --> 00:00:15,446
Facebook, Instagram, Twitter ve Reddit gibi
CrowdTangle kapsamındaki

4
00:00:15,446 --> 00:00:19,864
tüm platformlarda çalışır.

5
00:00:19,864 --> 00:00:23,628
Bu araç, yayıncıların genel eğilimleri
anlamasına ve hangi içeriklerin etkili olup

6
00:00:23,628 --> 00:00:29,260
hangilerinin olmadığını daha kolay
analiz etmesine yardımcı olur.

7
00:00:29,260 --> 00:00:33,900
Intelligence'ı kullanmak için,
üstteki arama çubuğuna hesabınızı girmeniz yeterlidir.

8
00:00:35,353 --> 00:00:40,949
Fotoğraf, bağlantı ve video gibi gönderi türlerine ve beğenme, yorum

9
00:00:40,949 --> 00:00:45,750
ve paylaşım gibi etkileşim türlerine göre sıralama yapabilirsiniz.

10
00:00:45,750 --> 00:00:52,059
Rekabet analizi gerçekleştirmek için
üstteki arama çubuğuna

11
00:00:52,059 --> 00:00:55,240
beş adede kadar farklı sayfa girebilirsiniz.

12
00:00:55,240 --> 00:01:00,704
Daha da önemlisi, bu raporu PDF olarak dışa aktarabilir

13
00:01:00,704 --> 00:01:03,687
veya CSV dosyası olarak indirebilirsiniz.

14
00:01:03,687 --> 00:01:08,118
Bu raporu, herkese açık bir bağlantıya dönüştürebilir

15
00:01:08,118 --> 00:01:10,054
ve CrowdTangle kullanıp kullanmamaları

16
00:01:10,054 --> 00:01:12,662
fark etmeksizin ekibinizle paylaşabilirsiniz.

17
00:01:14,197 --> 00:01:18,605
Sorularınız varsa,
sağ alttaki sohbet düğmesini kullanarak

18
00:01:18,605 --> 00:01:21,612
CrowdTangle ekibiyle iletişime geçmekten çekinmeyin.

19
00:01:21,612 --> 00:01:24,133
CrowdTangle'ı kullanırken eğleneceğinizi umarız.

